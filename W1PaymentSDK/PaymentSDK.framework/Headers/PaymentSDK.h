//
//  PaymentSDKManagers.h
//  PaymentSDK
//
//  Created by Viktor Bespalov on 01/12/15.
//  Copyright © 2015 WalletOne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HTMLtree.h"
#import "xpath.h"
#import "xpathInternals.h"

//! Project version number for PaymentSDKManagers.
FOUNDATION_EXPORT double PaymentSDKVersionNumber;

//! Project version string for PaymentSDKManagers.
FOUNDATION_EXPORT const unsigned char PaymentSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PaymentSDK/PublicHeader.h>

