//
//  ViewController.swift
//  SDKTest
//
//  Created by Viktor Bespalov on 21/06/16.
//  Copyright © 2016 WalletOne. All rights reserved.
//

import UIKit
import PaymentSDK
import PayCardsAcquiring

class ViewController: UIViewController, PaymentSDKDelegate {
    
    // MARK: - IBOutlets: Views
    
    // MARK: - IBOutlets: Controls
    
    // MARK: - IBOutlets: Constraints
    
    // MARK: - IBOutlets: Objects
    
    // MARK: - Properties
    
    var configurationSDK = ConfigurationSDK()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurationSDK.merchantId = NSNumber(unsignedLongLong: 191238281216)
        configurationSDK.merchantEDSKey = "6957347c416e466d397877614a5b3970574e55367c566278786548"
        configurationSDK.signatureMethodId = .MD5
        configurationSDK.culture = .EN
        configurationSDK.soundEnabled = true
        configurationSDK.saveCard = true
        configurationSDK.storeCVV = true
        configurationSDK.requeriedCardFields = PCDRequiredCardFields.All
        
        configurationSDK.paymentParams = {
            let paymentParams = PaymentParams()
            paymentParams.generateNewOrderID()
            paymentParams.amount = 12.34
            paymentParams.descriptionPayment = "Payment description for order \(paymentParams.orderId)"
            paymentParams.customer = Customer(customerId: "viktor.bespalov", firstName: "VIKTOR", lastName: "BESPALOV", middleName: "NIKOLAEVICH", email: "viktor.bespalov@walletone.com", phoneNumber: "9854619029")
            return paymentParams
        }()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        configurationSDK.paymentParams?.generateNewOrderID()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions

    @IBAction func presentSDK(sender: AnyObject) {
        PaymentSDKViewController.presentPaymentSDK(self, configuration: configurationSDK, delegate: self)
    }
    
    // MARK: - PaymentSDKDelegate
    
    func paymentSDKPaymentComplete(paymentSDKViewController: PaymentSDKViewController, paymentTypeId: PaymentTypeId, paymentStateId: PaymentStateId) {
        print("===   DemoViewController / paymentSDKPaymentComplete / paymentTypeId - \(paymentTypeId) / paymentStateId - \(paymentStateId)")
        
        paymentSDKViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func paymentSDKPaymentError(paymentSDKViewController: PaymentSDKViewController, paymentTypeId: PaymentTypeId, error: String?) {
        print("===   DemoViewController / paymentSDKError / paymentTypeId - \(paymentTypeId) / error - \(error)")
    }
}

